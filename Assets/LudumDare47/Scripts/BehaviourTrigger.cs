﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BehaviourTrigger : MonoBehaviour {
    bool started = false;
    public UnityEvent unityEvent;

    void OnTriggerEnter () {
        if (!started && unityEvent != null) {
            started = true;
            unityEvent.Invoke();
        }
    }
}
