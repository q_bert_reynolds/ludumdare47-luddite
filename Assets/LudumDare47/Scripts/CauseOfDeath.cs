﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Paraphernalia.Extensions;

public class CauseOfDeath : MonoBehaviour
{
    public Collider causeOfDeath;
    public GameObject deathPose;
    public void ShowDeathPose () {
        causeOfDeath.enabled = false;
        deathPose.SetActive(true);
    }
}
