﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DifficultySelect : MonoBehaviour
{
    public static bool isHardMode = true;
    public string sceneToLoad = "Game";
    bool loading = false;
    public bool anyKey = false;
    public string startButton = "Submit";
    public Text text;
    public Image transition;

    
    float lastX = 0;
    void Update()
    {
        if (Input.GetButtonDown(startButton)) {
            StartGame();
        }

        float x = Input.GetAxisRaw("Horizontal");
        if (Mathf.Abs(x) > 0.5f && Mathf.Abs(lastX) < 0.5f) {
            ToggleDifficulty(); 
        }
        lastX = x;

    }

    public void StartGame () {
        if (loading) return;
        loading = true;
        StartCoroutine(LoadScene());
    }

    public void ToggleDifficulty () {
        if (loading) return;
        isHardMode = !isHardMode;
        text.text = isHardMode ? "< HARD MODE >" : "< EASY MODE >";         
    }

    IEnumerator LoadScene () {
        Color startColor = transition.color;
        Color endColor = startColor;
        endColor.a = 1;
        for (float t = 0; t <= 1; t += Time.unscaledDeltaTime) {
            transition.color = Color.Lerp(startColor, endColor, t);
            yield return new WaitForEndOfFrame();
        }
        SceneManager.LoadScene(sceneToLoad);
    }
}
