﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using Paraphernalia.Components;
public class GameHUD : MonoBehaviour
{
    public static GameHUD hud;
    public static int deathCount;
    public Transform[] buttons;
    public Text deathCountText;
    public GameObject pauseScreen;
    public GameObject instructions;
    
    public static bool paused;
    void Awake() {
        if (hud == null) {
            hud = this;
            hud.deathCountText.text = "ˣ0";
            deathCount = 0;
        }
        buttons[1].GetComponentInChildren<Text>().text = instructions.activeSelf ? "Hide Instructions" : "Show Instructions";
        Resume(false);
    }

    void PauseResume() {
        if (paused) Resume();
        else Pause();
    }

    public void Pause()
    {
        AudioManager.PlayEffect("cancel");
        paused = true;
        Time.timeScale = 0;
        pauseScreen.SetActive(true);
        EventSystem.current.SetSelectedGameObject(buttons[0].gameObject);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void Resume(bool playSFX = true)
    {
        if (playSFX) AudioManager.PlayEffect("select");
        paused = false;
        Time.timeScale = 1;
        pauseScreen.SetActive(false);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
    

    public void ShowHideInstructions() {
        instructions.SetActive(!instructions.activeSelf);
        buttons[1].GetComponentInChildren<Text>().text = instructions.activeSelf ? "Hide Instructions" : "Show Instructions";
        AudioManager.PlayEffect(instructions.activeSelf ? "select" : "cancel");
    }

    public static void IncrementDeaths () {
        deathCount++;
        hud.deathCountText.text = "ˣ" + deathCount.ToString();
    }

    public void ExitToMenu()
    {
        Spawner.DisableAll();
        AudioManager.PlayEffect("cancel");
        SceneManager.LoadScene("Menu");
    }

    void Update ()
    {
        if (Input.GetButtonDown("Cancel"))
        { 
            PauseResume();
        }
    }
}
