﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabIKController : MonoBehaviour
{
    public bool grabbing;
    public Vector3 grabOffset;
    Animator anim;
    void Awake () {
        anim = GetComponent<Animator>();
    }    

    void OnAnimatorIK() {
        if (grabbing) {
            anim.SetIKPositionWeight(AvatarIKGoal.RightHand,1);
            anim.SetIKPosition(AvatarIKGoal.RightHand, transform.position + transform.forward * grabOffset.z + transform.right * grabOffset.x);
        }
        else {
            anim.SetIKPositionWeight(AvatarIKGoal.RightHand,0);
        }

    }
}
