﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Paraphernalia.Components;

public class GunController : MonoBehaviour
{

    ProjectileLauncher launcher;
    Animator anim;
    void Awake () {
        launcher = GetComponentInChildren<ProjectileLauncher>();
        anim = GetComponent<Animator>();
    }

    void OnTriggerEnter() {
        anim.SetBool("shoot", true);
    }

    void OnTriggerExit() {
        anim.SetBool("shoot", false);
    }

    public void Fire() {
        launcher.Shoot (launcher.transform.forward);

    }

}
