﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Paraphernalia.Extensions;
using Paraphernalia.Components;

[ExecuteInEditMode]
public class PlayerController : MonoBehaviour {
    public static PlayerController player;

    [Header("Life")]
    public bool dead = false;
    public LayerMask deathMask = 0;
    public ParticleSystem impactPoof;

    [Header("Camera")]
    public Transform camPivot;
    public Vector2 mouseSpeed = new Vector2(2.5f, 3);
    public float camMoveSpeed = 10;
    public float camTurnSpeed = 50;
    public float lookInfluence = 1;
    Camera cam;
    Transform camParent;

    [Header("Move")]
    public float forwardSpeed = 5;
    public float turnSpeed = 5;
    Vector3 targetVelocity;

    [Header("Grab")]
    public LayerMask grabbableMask = -1;
    public float grabDelay = 0.8f;
    float lastGrabTime;
    int currentGrab = 0;
    Vector3 grabOffset = new Vector3(0.3f, 0.8f, 1f);
    Rigidbody grabTarget;
    public bool grabbing { get { return grabTarget != null; } }
    GrabIKController grabIK;

    [Header("Jump")]
    [Range(0, 10)] public float jumpHeight = 5;
    [Range(0.01f, 0.3f)] public float jumpWindow = 0.05f;
    [Range(0.1f, 1)] public float jumpCancelHeight = 0.5f;
    float lastGroundedTime = -1;
    float lastJumpRequestTime = -1;
    float lastJumpTime = -1;

    [Header("Ground")]
    public LayerMask envMask = -1;
    [Range(0.001f, 0.25f)]public float groundHitRadius = 0.1f;
    bool onGround = false;
    public bool canJump { 
        get { 
            return (Time.time - lastGroundedTime) < jumpWindow
                && (Time.time - lastJumpTime) > jumpWindow*2
                && (Time.time - lastJumpRequestTime) < jumpWindow; 
        }
    }

    Animator anim;
    Rigidbody body;
    Vector3 startPosition;
    Quaternion startRotation;
    void Awake() {
        if (player == null) {
            player = this;
            anim = GetComponentInChildren<Animator>();
            body = GetComponent<Rigidbody>();
            cam = camPivot.GetComponentInChildren<Camera>();
            grabIK = GetComponentInChildren<GrabIKController>();

            camParent = cam.transform.parent;
            startPosition = transform.position;
            startRotation = transform.rotation;
        }
    }

    void Update() {
        if (!Application.isPlaying && camPivot != null) {
            camPivot.position = transform.position;
            cam.transform.rotation = Quaternion.LookRotation( transform.position + transform.forward * lookInfluence + Vector3.up - cam.transform.position);
            return;
        }

        CheckGround();
        if (GameHUD.paused) return;
        if (dead) targetVelocity = Vector3.zero;
        else UpdateAlive();

        // update animation params
        Vector3 v = body.velocity;
        v.y = 0;
        float speed = v.magnitude;
        anim.SetFloat("speed", speed);
        anim.SetFloat("forwardVelocity", Vector3.Dot(transform.forward, body.velocity));
        anim.SetFloat("yVelocity", body.velocity.y);
        anim.SetBool("onGround", onGround);

        
        // rotate camera
        float spin = Input.GetAxis("Horizontal2") + Input.GetAxis("Mouse X") * mouseSpeed.x;
        float rise = Input.GetAxis("Vertical2") + Input.GetAxis("Mouse Y") * mouseSpeed.y;
        camPivot.Rotate(Vector3.up * spin * camTurnSpeed * Time.unscaledDeltaTime);
        Vector3 p = camParent.transform.localPosition;
        p.y = Mathf.Clamp(p.y + rise * Time.unscaledDeltaTime * camMoveSpeed, 0.5f, 10f);
        camParent.transform.localPosition = p;

    }

    void UpdateAlive () {
        Vector3 v;
            
        // get input
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        if (Input.GetButtonDown("Jump")) lastJumpRequestTime = Time.time;
        if (canJump) {
            AudioManager.PlayVariedEffect("jump");
            anim.SetTrigger("jump");
            lastJumpTime = Time.time;
            v = body.velocity;
            v.y = Mathf.Sqrt(2 * jumpHeight * Physics.gravity.magnitude);
            targetVelocity.y = v.y;
            body.velocity = v;
        }

        // get desired velocity
        targetVelocity = camPivot.forward * y * forwardSpeed + camPivot.right * x * forwardSpeed;
        v = body.velocity;
        v.y = 0;
        float speed = v.magnitude;
        
        // get desired heading
        Vector3 targetDir = transform.forward;
        if (speed > 0.1f) targetDir = targetVelocity / speed;
        targetDir.y = 0;

        // rotate and update turn animation
        Vector3 turnCross = Vector3.up * Vector3.Cross(transform.forward, targetDir).y;
        transform.Rotate(turnCross * turnSpeed * Time.deltaTime * Mathf.Rad2Deg);

        if (!grabbing && Input.GetButtonDown("Fire1")) GrabClosest();
        else if (Input.GetButtonUp("Fire1")) {
            grabTarget = null;
            grabIK.grabbing = false;
        }
    }

    void CheckGround () {
        onGround = Physics.CheckSphere(transform.position + Vector3.down * groundHitRadius * 0.5f, groundHitRadius, envMask);
        if (onGround) lastGroundedTime = Time.time;
    }

    void GrabClosest () {
        grabIK.grabbing = false;
        if (Time.time - lastGrabTime < grabDelay) return;
        grabIK.grabbing = true;
        lastGrabTime = Time.time;
        AudioManager.PlayVariedEffect("grab");
        Collider[] targets = Physics.OverlapSphere(transform.position + transform.forward, 0.5f, grabbableMask);
        if (targets.Length > 0) {
            currentGrab %= targets.Length;
            grabTarget = targets[currentGrab].transform.GetComponent<Rigidbody>();
            grabOffset = transform.InverseTransformPoint(grabTarget.transform.position);
            grabOffset += Vector3.up * 0.5f;
            grabIK.grabOffset = grabOffset;
        }
        else grabTarget = null;
    }

    void FixedUpdate() {
        if (GameHUD.paused) return;
        Vector3 velocityChange = targetVelocity - body.velocity;
        velocityChange.y = 0;
        body.AddForce(velocityChange, ForceMode.VelocityChange);

        Vector3 targetCamPosition = transform.position;

        if (grabbing && !dead) {
            Vector3 targetGrabPosition = 
                transform.position + 
                transform.forward * grabOffset.z + 
                transform.right * grabOffset.x +
                transform.up * grabOffset.y;

            Vector3 v = body.velocity;
            v += (targetGrabPosition - grabTarget.transform.position).normalized * turnSpeed;
            v.y = 0;
            Vector3 diff = (grabTarget.transform.position - transform.position);
            v += diff.normalized * (1f/diff.sqrMagnitude);
            v = v - grabTarget.velocity;
            grabTarget.AddForce(v, ForceMode.VelocityChange);

            targetCamPosition += diff * 0.5f;
        }

        camPivot.position = Vector3.Lerp(camPivot.position, targetCamPosition, Time.fixedUnscaledDeltaTime * camMoveSpeed);
        Quaternion look = Quaternion.LookRotation(targetCamPosition + transform.forward * lookInfluence + Vector3.up - cam.transform.position);
        cam.transform.rotation = Quaternion.Slerp(cam.transform.rotation, look, Time.fixedDeltaTime * camMoveSpeed);
    }

    public float impactDeathVelocity = 26;
    void OnCollisionEnter (Collision c) {
        float deathVeloSqr = impactDeathVelocity * impactDeathVelocity;
        float vol = c.relativeVelocity.sqrMagnitude / deathVeloSqr;
        AudioManager.PlayEffect("impact", volume: vol, pitch: 0.7f);
        if (c.gameObject.InLayerMask(deathMask)) {
            CauseOfDeath cause =c.gameObject.GetComponentInParent<CauseOfDeath>();
            if (cause) {
                cause.ShowDeathPose();
                Die(false);
            }
            else {
                Die(true);
            }
        }        
        else if (c.relativeVelocity.sqrMagnitude > deathVeloSqr) {
            impactPoof.Play();
            Die(true);            
        }
    }

    void Die (bool makeRag) {
        if (dead) return;
        CameraShake.MainCameraShake();
        AudioManager.PlayVariedEffect("death");
        GameHUD.IncrementDeaths();
        dead = true;
        grabTarget = null;
        anim.gameObject.SetActive(false);
        body.isKinematic = true;
        body.useGravity = false;
        if (makeRag) {
            GameObject ragdoll = Spawner.Spawn("Ragdoll", false);
            ragdoll.transform.position = transform.position;
            ragdoll.transform.rotation = transform.rotation;
            ragdoll.SetActive(true);
        }
        Time.timeScale = 0.25f;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;
        StartCoroutine(Resurect());
    }
    
    IEnumerator Resurect() {
        yield return new WaitForSecondsRealtime(1);
        AudioManager.PlayEffect("resurect");
        ParticleManager.Play("Puff", transform);
        dead = false;
        grabTarget = null;
        if (DifficultySelect.isHardMode) {
            transform.position = startPosition;
            camPivot.position = startPosition;
            transform.rotation = startRotation;
        }
        anim.gameObject.SetActive(true);
        body.isKinematic = false;
        body.useGravity = true;
        if (GameHUD.paused) Time.timeScale = 0;
        else  Time.timeScale = 1;
        Time.fixedDeltaTime = 0.02f;
    }
}
