﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Paraphernalia.Components;

public class SoundPlayer : MonoBehaviour
{
    public void PlaySound (string name) {
        AudioManager.PlayVariedEffect(name);
    }
}
